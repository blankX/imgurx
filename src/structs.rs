//use std::fmt;
use serde::Deserialize;
extern crate reqwest;
extern crate serde_json;

#[derive(Debug, Deserialize)]
pub struct Album {
    pub title: String,
    pub is_mature: bool,
    pub media: Vec<Media>,
}

#[derive(Debug, Deserialize)]
pub struct Media {
    pub r#type: String,
    pub url: String,
    pub width: u32,
    pub height: u32,
    pub metadata: Metadata,
}

#[derive(Debug, Deserialize)]
pub struct Metadata {
    pub description: String,
    pub duration: f32,
}

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    SerdeJSON(serde_json::Error),
    APIErrors(APIErrors),
}

impl From<reqwest::Error> for Error {
    #[inline]
    fn from(error: reqwest::Error) -> Error {
        Error::Reqwest(error)
    }
}

impl From<serde_json::Error> for Error {
    #[inline]
    fn from(error: serde_json::Error) -> Error {
        Error::SerdeJSON(error)
    }
}

#[derive(Debug, Deserialize)]
pub struct APIErrors {
    pub errors: Vec<APIError>,
}

#[derive(Debug, Deserialize)]
pub struct APIError {
    pub id: String,
    pub code: String,
    pub status: String,
    pub detail: String,
}
